Terraform when destroy
rm -rf .terraform*
rm -rf terraform.tfstate*

Open Jenkins-Master --> ssh-keygen
Open Jenkins-Agent --> add id_rsa.pub (from Jenkins-Master -> /home/ubuntu/.ssh/id_rsa.pub) --> /home/ubuntu/.ssh/authorized_keys

Open web Jenkins --> Manage Jenkins --> Nodes --> Built-In Node --> Configure --> Number of executors = 0
--> Dashboard --> Manage --> Jenkins --> Nodes --> New node {"Node name": "Jenkins-Agent", "Permanent Agent": true} -> Create
--> {
  "Number of executors": 2,
  "Remote root directory": "/home/ubuntu",
  "labels": "Jenkins-Agent",
  "Launch method": "Launch agents via SSH",
  "Host": -> Private IPv4 addresses of Jenkins-Agent,
  "Credentials": -> Add -> Jenkins -> {
    "Kind": "SSH Username with private key",
    "ID": Jenkins-Agent,
    "Username": "ubuntu",
    "Private Key": -> copy /home/ubuntu/.ssh/id_rsa of Jenkins-Master
  },
  "Host Key Verification Strategy": "Non verifying Verification Strategy"
}

Manage Jenkins Plugins:
Maven Integration
Pipeline Maven Integration
Eclipse Temurin installer

Manage Jenkins Tools:
Maven installations -> Add Maven {"Name": "Maven3", "Install automatically": True}
JDK installations -> Add JDK {"Name": "Java17", "Install automatically": True -> Install from adoptium.net -> select version}

Manage Jenkins Credentials:
Stores scoped to Jenkins -> add credentials
