locals {
  # VPC local variables

  # SECURITY GROUP local variables
  sg_vpc_id = module.vpc-module.vpc_id

  # EC2 local variables
  ec2_security_group_ids = [module.security-group-module.sg_security_group_id]
  ec2_subnet_id          = [
    element(module.vpc-module.vpc_public_subnets, 0),
    element(module.vpc-module.vpc_public_subnets, 1)
  ]
  ec2_user_data          = [
    file("${path.module}/scripts/jenkins-master-install.sh"),
    file("${path.module}/scripts/jenkins-agent-install.sh")
  ]
}
