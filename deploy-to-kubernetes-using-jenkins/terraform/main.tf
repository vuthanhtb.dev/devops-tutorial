module "vpc-module" {
  source = "./modules/vpc"
}

module "security-group-module" {
  source = "./modules/security-group"

  depends_on = [module.vpc-module]
  sg_vpc_id  = local.sg_vpc_id
}

module "ec2-module" {
  source = "./modules/ec2"

  depends_on = [
    module.vpc-module,
    module.security-group-module
  ]
  ec2_subnet_id          = local.ec2_subnet_id
  ec2_security_group_ids = local.ec2_security_group_ids
  ec2_user_data          = local.ec2_user_data
}
