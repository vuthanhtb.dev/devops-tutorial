module "ec2-instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "5.5.0"

  count = length(var.ec2_list)

  name                        = var.ec2_list[count.index]

  ami                         = var.ec2_ami[count.index]
  instance_type               = var.ec2_instance_type[count.index]
  key_name                    = var.ec2_key_name[count.index]
  monitoring                  = var.ec2_monitoring
  vpc_security_group_ids      = var.ec2_security_group_ids
  subnet_id                   = var.ec2_subnet_id[count.index]
  associate_public_ip_address = var.ec2_associate_public_ip_address[count.index]
  user_data                   = var.ec2_user_data[count.index]
  tags                        = var.ec2_tags[count.index]

  ebs_block_device            = var.ebs_block_device
}
