variable "ec2_list" {
  description = "A list of name ec2"
  type        = list(string)
  default     = ["jenkins-master", "jenkins-agent"]
}

variable "ec2_ami" {
  description = "ID of AMI to use for the instance"
  type        = list(string)
  default     = ["ami-03caf91bb3d81b843", "ami-03caf91bb3d81b843"] # Canonical, Ubuntu, 20.04 LTS, amd64 focal image build on 2023-10-25
}

variable "ec2_instance_type" {
  description = "The type of instance to start"
  type        = list(string)
  default     = ["t2.micro", "t2.micro"]
}

variable "ec2_key_name" {
  description = "Key name of the Key Pair to use for the instance; which can be managed using the `aws_key_pair` resource"
  type        = list(string)
  default     = ["devops", "devops"]
}

variable "ec2_monitoring" {
  description = "If true, the launched EC2 instance will have detailed monitoring enabled"
  type        = bool
  default     = null
}

variable "ec2_associate_public_ip_address" {
  description = "Whether to associate a public IP address with an instance in a VPC"
  type        = list(bool)
  default     = [true, false]
}

variable "ec2_subnet_id" {
  description = "The VPC Subnet ID to launch in"
  type        = list(string)
  default     = []
}

variable "ec2_security_group_ids" {
  description = "A list of security group IDs to associate with"
  type        = list(string)
  default     = []
}

variable "ec2_user_data" {
  description = "The user data to provide when launching the instance. Do not pass gzip-compressed data via this argument; see user_data_base64 instead"
  type        = list(string)
  default     = []
}

variable "ec2_tags" {
  description = "A mapping of tags to assign to the resource"
  type        = list(map(string))
  default     = [
    {Name = "jenkins-master"},
    {Name = "jenkins-agent"}
  ]
}

variable "ebs_block_device" {
  description = "Additional EBS block devices to attach to the instance"
  type        = list(any)
  default     = [
    {
      device_name = "/dev/sda1"
      volume_type = "gp2"
      volume_size = 20
    }
  ]
}
