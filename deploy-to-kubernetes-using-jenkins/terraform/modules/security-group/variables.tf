variable "sg_name" {
  description = "Name of security group"
  type        = string
  default     = "jenkins-sg"
}

variable "sg_description" {
  description = "Description of security group"
  type        = string
  default     = "Security Group managed by Terraform"
}

variable "sg_vpc_id" {
  description = "ID of the VPC where to create security group"
  type        = string
}

variable "sg_ingress_cidr_blocks" {
  description = "List of IPv4 CIDR ranges to use on all ingress rules"
  type        = list(any)
  default     = ["0.0.0.0/0"]
}

variable "sg_ingress_rules" {
  description = "List of ingress rules to create by name"
  type        = list(any)
  default     = []
}

variable "sg_ingress_with_cidr_blocks" {
  description = "List of ingress rules to create where 'cidr_blocks' is used"
  type        = list(any)
  default     = [
    {
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      description = "jenkins ports"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
}

variable "sg_egress_rules" {
  description = "List of egress rules to create by name"
  type        = list(any)
  default     = ["all-all"]
}

variable "sg_tags" {
  description = "A mapping of tags to assign to security group"
  type        = map(string)
  default     = {Name = "jenkins security group"}
}
