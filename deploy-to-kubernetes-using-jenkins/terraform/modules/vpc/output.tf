output "vpc_id" {
  description = "The ID of the VPC"
  value       = module.vpc.vpc_id
}

output "vpc_public_subnets" {
  description = "List of IDs of public subnets"
  value       = [for sn in module.vpc.public_subnets: sn]
}

output "vpc_private_subnets" {
  description = "List of IDs of private subnets"
  value       = [for sn in module.vpc.private_subnets: sn]
}
