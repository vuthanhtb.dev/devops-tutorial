#!/bin/bash
sudo apt update -y
sudo apt upgrade -y

sudo echo "Jenkins-Agent" | sudo tee /etc/hostname

sudo apt install openjdk-17-jre -y

sudo apt-get install docker.io -y
sudo usermod -aG docker $USER

sudo sed -i 's/#PubkeyAuthentication yes/PubkeyAuthentication yes/g' /etc/ssh/sshd_config
sudo sed -i 's/#AuthorizedKeysFile/AuthorizedKeysFile /g' /etc/ssh/sshd_config
sudo service sshd reload

# Restart machine
sudo init 6
