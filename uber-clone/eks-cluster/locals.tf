locals {
  aws_region = "ap-southeast-1"
  # VPC local variables
  vpc_name               = "uber-clone-vpc"
  vpc_cidr_block         = "10.10.0.0/16"
  vpc_availability_zones = ["ap-southeast-1a", "ap-southeast-1b"]
  vpc_public_subnets     = ["10.10.101.0/24", "10.10.102.0/24"]
  vpc_private_subnets    = ["10.10.103.0/24", "10.10.104.0/24"]

  # EKS local variables
  eks_cluster_name                    = "uber-eks"
  eks_cluster_version                 = "1.27"
  eks_cluster_endpoint_private_access = true
  eks_cluster_endpoint_public_access  = true
  eks_vpc_id                          = module.vpc-uber-clone-module.vpc_id
  eks_subnet_ids                      = module.vpc-uber-clone-module.vpc_private_subnets
  # eks_vpc_id      = "vpc-01905e77784c4a5f9"
  # eks_subnet_ids  = ["10.10.103.0/24", "10.10.104.0/24"]
  eks_enable_irsa = true
  eks_managed_node_group_defaults = {
    disk_size = 50
  }
  eks_managed_node_groups = {
    general = {
      desired_size = 1
      min_size     = 1
      max_size     = 10

      labels = {
        role = "general"
      }

      instance_types = ["t2.micro"]
      capacity_type  = "ON_DEMAND"
    }

    spot = {
      desired_size = 1
      min_size     = 1
      max_size     = 10

      labels = {
        role = "spot"
      }

      taints = [{
        key    = "market"
        value  = "spot"
        effect = "NO_SCHEDULE"
      }]

      instance_types = ["t2.micro"]
      capacity_type  = "SPOT"
    }
  }
  eks_tags = {
    Name = "Uber EKS"
  }
}
