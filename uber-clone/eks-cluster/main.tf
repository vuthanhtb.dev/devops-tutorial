module "vpc-uber-clone-module" {
  source = "../terraform/modules/vpc"

  vpc_name               = local.vpc_name
  vpc_cidr_block         = local.vpc_cidr_block
  vpc_availability_zones = local.vpc_availability_zones
  vpc_public_subnets     = local.vpc_public_subnets
  vpc_private_subnets    = local.vpc_private_subnets
}

module "eks-user-module" {
  source = "../terraform/modules/eks"

  # depends_on = [
  #   module.vpc-uber-clone-module
  # ]

  eks_cluster_name                    = local.eks_cluster_name
  eks_cluster_version                 = local.eks_cluster_version
  eks_cluster_endpoint_private_access = local.eks_cluster_endpoint_private_access
  eks_cluster_endpoint_public_access  = local.eks_cluster_endpoint_public_access
  eks_vpc_id                          = local.eks_vpc_id
  eks_subnet_ids                      = local.eks_subnet_ids
  eks_enable_irsa                     = local.eks_enable_irsa
  eks_managed_node_group_defaults     = local.eks_managed_node_group_defaults
  eks_managed_node_groups             = local.eks_managed_node_groups
  eks_tags                            = local.eks_tags
}
