locals {
  aws_region = "ap-southeast-1"

  # VPC local variables
  vpc_name               = "uber-clone-vpc"
  vpc_cidr_block         = "10.10.0.0/16"
  vpc_availability_zones = ["ap-southeast-1a", "ap-southeast-1b"]
  vpc_public_subnets     = ["10.10.101.0/24", "10.10.102.0/24"]
  vpc_private_subnets    = ["10.10.103.0/24", "10.10.104.0/24"]

  # SECURITY GROUP local variables
  sg_vpc_id        = module.vpc-uber-clone-module.vpc_id
  sg_ingress_rules = ["ssh-tcp"]
  sg_tags          = { Name = "master security group" }
  sg_name          = "master-sg"
  sg_ingress_with_cidr_blocks = [
    {
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      description = "jenkins port"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = 9090
      to_port     = 9090
      protocol    = "tcp"
      description = "sonar port"
      cidr_blocks = "0.0.0.0/0"
    }
  ]

  # EC2 local variables
  ec2_name                        = "master"
  ec2_ami                         = "ami-03caf91bb3d81b843"
  ec2_instance_type               = "t2.large"
  ec2_key_name                    = "devops"
  ec2_associate_public_ip_address = true
  ec2_tags                        = { Name = "master" }
  ec2_security_group_ids          = [module.sg-master-module.sg_security_group_id]
  ec2_subnet_id                   = element(module.vpc-uber-clone-module.vpc_public_subnets, 0)
  ec2_user_data                   = file("${path.module}/scripts/master.sh")
}
