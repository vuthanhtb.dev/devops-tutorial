module "vpc-uber-clone-module" {
  source = "../terraform/modules/vpc"

  vpc_name               = local.vpc_name
  vpc_cidr_block         = local.vpc_cidr_block
  vpc_availability_zones = local.vpc_availability_zones
  vpc_public_subnets     = local.vpc_public_subnets
  vpc_private_subnets    = local.vpc_private_subnets
}

module "sg-master-module" {
  source = "../terraform/modules/security-group"

  depends_on = [module.vpc-uber-clone-module]

  sg_vpc_id                   = local.sg_vpc_id
  sg_ingress_rules            = local.sg_ingress_rules
  sg_ingress_with_cidr_blocks = local.sg_ingress_with_cidr_blocks
  sg_tags                     = local.sg_tags
  sg_name                     = local.sg_name
}

module "ec2-master-module" {
  source = "../terraform/modules/ec2"

  depends_on = [
    module.vpc-uber-clone-module,
    module.sg-master-module
  ]

  ec2_subnet_id                   = local.ec2_subnet_id
  ec2_security_group_ids          = local.ec2_security_group_ids
  ec2_user_data                   = local.ec2_user_data
  ec2_name                        = local.ec2_name
  ec2_ami                         = local.ec2_ami
  ec2_instance_type               = local.ec2_instance_type
  ec2_key_name                    = local.ec2_key_name
  ec2_associate_public_ip_address = local.ec2_associate_public_ip_address
  ec2_tags                        = local.ec2_tags
}
