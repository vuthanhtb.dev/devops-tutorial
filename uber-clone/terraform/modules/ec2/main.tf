module "ec2-instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "5.5.0"

  name                        = var.ec2_name
  ami                         = var.ec2_ami
  instance_type               = var.ec2_instance_type
  key_name                    = var.ec2_key_name
  monitoring                  = var.ec2_monitoring
  vpc_security_group_ids      = var.ec2_security_group_ids
  subnet_id                   = var.ec2_subnet_id
  associate_public_ip_address = var.ec2_associate_public_ip_address
  user_data                   = var.ec2_user_data
  tags                        = var.ec2_tags
  ebs_block_device            = var.ec2_ebs_block_device
}
