output "sg_security_group_id" {
  description = "The ID of the security group"
  value       = module.security_group.security_group_id
}
# output "security_group_id" {
#   description = "The ID of the security group"
#   value       = try(module.security_group.security_group_id, module.security_group.this_name_prefix[0].id, "")
# }