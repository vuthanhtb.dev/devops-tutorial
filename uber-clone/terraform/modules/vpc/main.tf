module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.1.2"

  # VPC Basic Details
  name            = var.vpc_name
  cidr            = var.vpc_cidr_block
  azs             = var.vpc_availability_zones
  public_subnets  = var.vpc_public_subnets
  private_subnets = var.vpc_private_subnets

  # Database Subnets
  database_subnets                       = var.vpc_database_subnets
  create_database_subnet_group           = var.vpc_create_database_subnet_group
  create_database_subnet_route_table     = var.vpc_create_database_subnet_route_table
  create_database_internet_gateway_route = var.vpc_create_database_internet_gateway_route
  create_database_nat_gateway_route      = var.vpc_create_database_nat_gateway_route

  # NAT Gateways - Outbound Communication
  enable_nat_gateway = var.vpc_enable_nat_gateway
  single_nat_gateway = var.vpc_single_nat_gateway

  # VPC DNS Parameters
  enable_dns_hostnames = var.vpc_enable_dns_hostnames
  enable_dns_support   = var.vpc_enable_dns_support

  #IPV6
  enable_ipv6 = var.vpc_enable_ipv6

  tags     = var.tags
  vpc_tags = var.vpc_tags

  # Additional Tags to Subnets
  public_subnet_tags   = var.vpc_public_subnet_tags
  private_subnet_tags  = var.vpc_private_subnet_tags
  database_subnet_tags = var.vpc_database_subnet_tags
}
