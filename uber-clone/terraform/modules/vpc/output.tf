output "vpc_id" {
  value       = module.vpc.vpc_id
  description = "VPC ID"
}

output "vpc_public_subnets" {
  value       = module.vpc.public_subnets
  description = "VPC public subnets' IDs list"
}

output "vpc_private_subnets" {
  value       = module.vpc.private_subnets
  description = "VPC private subnets' IDs list"
}
